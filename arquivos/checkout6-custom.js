$(window).on('load', function () {
    $('h2.empty-cart-title').text('');
    $('h2.empty-cart-title').text('Carrinho Vazio');

    $('.empty-cart-message p').text('');
    $('.empty-cart-message p').text('Parece que o seu carrinho está vazio. navegue pelas categorias do site ou faça uma busca.');

    if ($('.empty-cart-content').attr('style') === 'display: block;') {
        $('#cart-title').addClass('hiden');
        $('body.body-cart .cart-template-holder').addClass('active');
    }
});
      
$(function () {
    $("#cart-to-orderform, #payment-data-submit").hide();
    $.ajax({
      url: "/api/vtexid/pub/authenticated/user",
      type: "GET",
    }).done(function (response) {
      if (response != null) {
        $.ajax({
          type: "GET",
          dataType: "json",
          url:
            "/api/dataentities/CL/search?_fields=bloquearPedido%2Cblacklist&email=" +
            response.user,
        }).done(function (res) {
          if (res.length == 1) {
            console.log(res[0]);
            if (res[0].bloquearPedido == true) {
              $("#cart-to-orderform, #payment-data-submit").remove();
            }
            if (res[0].bloquearPedido == "T") {
                $("#continue_buy .footer ul li:nth-child(2)").remove();
            }
          }else{
            $("#cart-to-orderform, #payment-data-submit").show();
          }
        });
      }else{
        $("#cart-to-orderform, #payment-data-submit").show();
      }
    });
  });